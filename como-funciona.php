<?php require 'header.php'; ?>
	<div id="landing-carousel">
	    <div class="item">
	    	<div class="title">
	    	<div class="prev" style="margin:0 auto; width: 940px;">
	    	
	    		<p><h4>Rizer é um ferramenta que utiliza a técnica de Mapeamento objeto-relacional ou (ORM, do inglês: Object-relational mapping) para gerar sistemas completos a partir da estrutura do banco de dados, ou seja, a partir das tabelas existentes no banco de dados.</h4></p>
	    		
	    		<p><h4>A ferramenta tem suporte aos principais bancos de dados, entre eles: MySQL , PostgreSQL , Oracle e SQL Server. Qualquer aplicação gerada funciona independentemente do Rizer e pode ou podem ser publicadas em qualquer servidor web PHP.</h4></p>

	    		<p><h4>O objetivo da plataforma é disponibizar uma alternativa simples para a geração de sistemas completos utilizando o que há de mais moderno em ferramentas para desenvolvimento de software, reduzindo tempo e consequentemente custos.</h4></p>
	    
	    		<p><h4>Utilizar a ferramenta é muito simples, basta apenas informar os dados de conexão ao seu servidor de hospedagem ou efetuar o carregamento do script do seu banco de dados para que a ferramenta possa disponibilizar o seu sistema para download.</h4></p>
	    	</div>
	    </div>
	</div>
<?php require 'footer.php'; ?>