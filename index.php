<?php require 'header.php'; ?>

    <div class="title">
        <h1 style="width: 800px; margin-top:50px;margin-left: auto;margin-right: auto;">O código fonte do seu projeto em minutos!</h1>
        <p>Rizer é uma ferramenta fantástica capaz de gerar um sistema funcional utlizando apenas a modelagem do seu banco de dados.</p>
        <div class="arrow">
            <p class="home-try-it-for-free" style="text-align: left;">Experimente a versão beta <span class="orange">gratuitamente</span></p>
            <i class="icon-arrow-top-right" style="position: relative;top: -20px;left: 95px;"></i>
        </div>
    </div>

    <div style="width:400px; background: #fff; margin-top: 20px; margin-left:auto; margin-right:auto; text-align:left;">
        <div id="success" class="alert alert-success" style="display:none;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Sucesso!</h4>
            <span></span>
        </div>
        <div id="error" class="alert alert-danger" style="display:none;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>Erro!</h4>
            <span></span>
        </div>
        <div class="bs-docs-example">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a href="#conexao" data-toggle="tab">Conectar</a></li>
                <li class=""><a href="#carregar" data-toggle="tab">Carregar</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="conexao">
                <br>
                    <form rule="form" action="#" method="post" id="test-conexao">
                        <div class="form-group">
                            <label><strong>Servidor:</strong></label>
                            <input type="text" name="server" class="form-control" placeholder="Informe o servidor" required>
                        </div>
                        <div class="form-group">
                            <label><strong>Usuário:</strong></label>
                            <input type="text" name="user" class="form-control" placeholder="Informe o usuário" required>
                        </div>
                        <div class="form-group">
                            <label><strong>Senha:</strong></label>
                            <input type="password" name="password" class="form-control" placeholder="Informe a senha" required>
                        </div>
                        <div class="form-group">
                            <label><strong>Banco de dados:</strong></label>
                            <input type="text" name="database" class="form-control" placeholder="Informe o nome do banco de dados" required>
                        </div>
                        <button type="submit" class="btn btn-info" id="testar-conexao" style="background-color:#34495e; border:#34495e;">Testar conexão</button>
                        <a href="./confirmar-conexao" class="btn btn-primary" id="confirmar-conexao" style="display:none;">Gerar sistema</a>
                        <a href="#" class="btn btn-primary" id="cancelar-conexao" style="display:none;">Cancelar</a>
                    </form>  
                </div>
                <div class="tab-pane fade" id="carregar">
                <br>
                    <form rule="form" action="#" id="importar" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label><strong>Banco de dados:</strong></label>
                            <input type="file" name="sql_dump" class="form-control" required>
                        </div>
                        <button type="submit" id="importar-sql" class="btn btn-primary">Importar</button>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('#myTab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        })
    </script>

<?php require 'footer.php'; ?>