<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="assets/img/rizerfavicon.png"/>
    <title>Rizer</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>
<body>
    <div id="fb-root"></div>
    <div class="header-bar navbar light">
        <div class="navbar-inner">
        <div class="container">
                <ul class="nav pull-right"> 
                    <li><a class="link" href="como-funciona.php">Como funciona?</a></li>
                    <li><a class="link" href="qual-a-vantagem.php">Qual a vantagem?</a></li>
                    <li><a class="link" href="documentacao.php">Documentação</a></li>
                </ul>
                <a class="brand" href="#" style="background-image: url(assets/img/rizer.png);"></a>
            </div>
        </div>
    </div>
